package ve.com.disinglab.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var dice : ImageView
    lateinit var rollButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rollButton = findViewById(R.id.roll_button)
        dice  = findViewById(R.id.dice_image)



        rollButton.setOnClickListener {
            rollDice()
        }

    }

    private fun rollDice(){
        val randomInt : Int = (1..6).random()

        val dice_result = when (randomInt) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        Toast.makeText(this, "Rolled", Toast.LENGTH_SHORT).show()

        dice.setImageResource(dice_result)

    }
}
